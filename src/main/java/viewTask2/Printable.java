package viewTask2;

@FunctionalInterface
public interface Printable {

    void print();
}
