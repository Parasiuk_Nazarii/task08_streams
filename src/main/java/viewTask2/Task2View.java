package viewTask2;

import task2.Command;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Task2View {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private Command cmd = new Command();

    public Task2View() {

        menu = new LinkedHashMap<>();
        menu.put("0", "  0 - Input name");
        menu.put("1", "  1 - says something quite");
        menu.put("2", "  2 - says something normal");
        menu.put("3", "  3 - says something loud");
        menu.put("4", "  4 - scream!!!");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("0", this::pressButton0);
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }

    private void pressButton0() {
        System.out.println("Input name please");
        String name = input.next();
        cmd.setName(name);
    }

    private void pressButton1() {
        cmd.whispers(cmd.ask());
    }

    private void pressButton2() {
        cmd.says(cmd.ask());
    }

    private void pressButton3() {
        cmd.saysLoud(cmd.ask());
    }

    private void pressButton4() {
        cmd.screams(cmd.ask());
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
