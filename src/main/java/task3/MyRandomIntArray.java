package task3;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class MyRandomIntArray {
    private int[] arr;

    private Random random = new Random();

    public int[] generate(int length, int bottom, int top) {
        if (length < 0 || bottom > 0) {
            throw new IllegalArgumentException("length must be > 0 and top> bottom");
        }
        int[] temp = new int[length];
        for (int i = 0; i < length; i++) {
            temp[i] = random.nextInt(top - bottom + 1) + bottom;
        }
        arr = temp;
        return temp;
    }

    public int sum() {
        return stream().sum();
    }

    public int sumReduce() {
        return stream().reduce(0, (a, b) -> a + b);
    }

    public double average() {

        return stream().average().getAsDouble();
    }

    public int min() {
        return stream().min().getAsInt();
    }

    public int max() {
        return stream().max().getAsInt();
    }

    public int countBiggerAvg() {
        double avg = average();
        return (int) stream().filter(x -> x > avg).count();
    }

    public void print() {
        System.out.println("This array:");
        stream().forEach(x -> System.out.print(x + "; "));
    }

    public IntStream stream() {
        return Arrays.stream(arr);
    }
}
