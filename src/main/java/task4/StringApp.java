package task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class StringApp {
    private List<String> list = new ArrayList<>();
    private Scanner input = new Scanner(System.in);

    public void input() {
        String word;
        System.out.println("Let's start");
        while (!(word = input.nextLine()).isEmpty()) {
            System.out.println("Input word:");
            list.add(word);
        }
    }

    public void print() {
        list.forEach((s -> System.out.print(s + "; ")));
    }

    public List<String> uniqWords() {
        List<String> str;
        str = list.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(str);
        return str;
    }

    public int numberOfUniqWords() {
        return (int) uniqWords().stream().count();
    }

    public Map<String, Long> countOfWords() {
        Map<String, Long> str;
        str = list.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .collect(groupingBy(Function.identity(), counting()));
        System.out.println(str);
        return str;
    }
}
