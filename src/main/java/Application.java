import task3.MyRandomIntArray;
import task4.StringApp;
import viewTask2.Task2View;

public class Application {
    public static void main(String[] args) {
        System.out.println("-----<First Task>-----");
        MyFunctionalInterface myfi1 = (a, b, c) -> (Math.max(a, Math.max(b, c)));
        MyFunctionalInterface myfi2 = (a, b, c) -> (a + b + c) / 3;
        int a = -2;
        int b = 2;
        int c = 10;
        System.out.println("Max value for " + a + "; " + b + "; " + c + "; " + "- > " + myfi1.function(a, b, c));
        System.out.println("Average value for " + a + "; " + b + "; " + c + "; " + "- > " + myfi2.function(a, b, c));

        System.out.println("-----<Second Task>-----");
        new Task2View().show();

        System.out.println("-----<Third Task>-----");
        MyRandomIntArray arr = new MyRandomIntArray();
        arr.generate(20, -100, 100);
        arr.print();
        System.out.println("\nSum = " + arr.sum() + " or " + arr.sumReduce());
        System.out.println("Average = " + arr.average());
        System.out.println("Min=" + arr.min() + "; Max=" + arr.max());
        System.out.println("Count of numbers bigger then average =" + arr.countBiggerAvg());

        System.out.println("-----<Fourth Task>-----");
        StringApp sapp = new StringApp();
        sapp.input();
        sapp.print();
        sapp.uniqWords();
        sapp.countOfWords();

    }

}
